// SPDX-FileCopyrightText: 2022 Lars Wirzenius <liw@liw.fi>
// SPDX-License-Identifier: MIT OR Apache-2.0

#![allow(clippy::needless_return)]

include!(concat!(env!("OUT_DIR"), "/opgpcard.rs"));
